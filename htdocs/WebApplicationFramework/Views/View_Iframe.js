function CView_Iframe(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";
    
    /** constructor */
    
    var v_mainId = p_mainId;
    var v_parentId = p_parentId;
    var v_customData = p_data;

    var v_viewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_Iframe)[0];
    var v_conditionViewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_Iframe)[1];
    
    var v_currentUrl = urlifier(v_customData.ip, v_customData.port, v_customData.relativeURL);
    var v_this = this;

    /** public functions */

    this.applicationCreated = function() {
        $("#" + v_parentId).append(getHtml());
        if (v_customData.isElementLabelPresent) {
            ViewUtils.addLabel(v_mainId, v_customData.elementText);
        }
        if (v_customData.class != undefined) {
            $("#" + v_mainId).addClass(v_customData.class);
        }
    };

    this.refresh = function() {   
        if (ViewUtils.checkVisibility(v_conditionViewmodel, v_mainId) && v_viewmodel != undefined && v_viewmodel.getList() != undefined) {
            var dataObject = v_viewmodel.getList().values;
            var vl_ip;
            if (dataObject == undefined || dataObject[0] == undefined) {
                return;
            }
            if (dataObject.length>0 && dataObject[0].length>0 && dataObject[0][0].length>0) {
              vl_ip = dataObject[0][0][0];
            }
            var vl_port;
            if (dataObject.length>1 && dataObject[1].length>0 && dataObject[1][0].length>0) {
              vl_port=dataObject[1][0][0]
            }
            var refreshedUrl = urlifier(vl_ip, vl_port, v_customData.relativeURL);

            if (v_currentUrl != refreshedUrl) {
                v_currentUrl = refreshedUrl;
                $("#" + v_mainId).replaceWith(getHtml());
            }
        }
    };

    /** private functions */
    
    function getHtml() {
        return '<iframe id="' + v_mainId + '" src="' + v_currentUrl + '"></iframe>';
    }
}

CView_Iframe.getHelp = function() {
    return "An iframe element. Any kind of locally avaliable url can be inserted as an iframe, by using a list input with the ip or url as the first member and port as optional second member.";
}

CView_Iframe.expectsInterface = function() {
    return [
        {
            "optional": ["getList"]
        }
    ];
};

CView_Iframe.getCustomDataSchema = function() {
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CView_Iframe",
        "type": "object",
        "properties": {
            "class": {
                "type": "string",
                "description": "the css class of the iframe"
            },
            "ip": {
                "type": "string",
                "description": "the address (ip or url) of the iframe"
            },
            "port": {
                "type": "string",
                "description": "the optional port of the iframe"
            },
            "relativeURL": {
                "description": "If true: current address is used as the base URL. Effective only when ip/port is not specified (default true)",
                "type": "boolean",
                "format": "checkbox",
                "default": true
            }
        },
        "additionalProperties": false
    };
    $.extend(true, schema, ViewUtils.commonViewSchema, ViewUtils.commonElementLabelSchema);
    return schema;
};
//# sourceURL=WebApplicationFramework\Views\View_Iframe.js