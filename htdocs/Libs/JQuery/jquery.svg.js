;(function ($) {
    "use strict";
    $.fn.createSVGNode = function (name, attrs) {
        var elName = (name !== undefined && typeof name === "string") ? name : "svg";
        var elAttrs = (attrs !== undefined && attrs instanceof Object) ? attrs : ((name instanceof Object) ? name : {});
        var el = document.createElementNS("http://www.w3.org/2000/svg", elName);
        for (var attr in elAttrs)
            if (elAttrs.hasOwnProperty(attr))
                el.setAttribute(attr, elAttrs[attr]);
        return $(this.get(0).appendChild(el));
    };
}(jQuery));
